
package com.mycompany.usermanagement;

import java.io.Serializable;

/**
 *
 * @author thorn
 */
public class User implements Serializable{
    private String loginName;
    private String password;
    
    public User(String loginName, String password){
        this.loginName = loginName;
        this.password = password;
    }

    public String getLoginName() {
        return loginName;
    }

    public String getPassword() {
        return password;
    }
    
    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" + "loginName=" + loginName + ", password=" + password + '}';
    }
    
}
